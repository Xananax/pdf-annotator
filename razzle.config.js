"use strict";
module.exports = {
  modifyWebpackOptions({
    env: {
      target, // the target 'node' or 'web'
      dev, // is this a development build? true or false
    },
    options: {
      webpackOptions, // the default options that will be used to configure webpack/ webpack loaders and plugins
    },
  }) {
    webpackOptions.notNodeExternalResMatch = (request, context) => {
      return /react-date-picker|anothermodule/.test(request);
    };
    webpackOptions.babelRule.include = webpackOptions.babelRule.include.concat([
      /react-date-picker/,
      /anothermodule/,
    ]);
    return webpackOptions;
  },
};
