import PDFJS from "pdfjs-dist/legacy/build/pdf.js";
import fs from "fs";

const silent = (err) => console.error(err);

const PDF = async (dataBuffer) => {
  let doc = await PDFJS.getDocument(dataBuffer).promise;

  const { metadata, info } = await doc.getMetadata().catch(silent);

  const render_options = {
    normalizeWhitespace: false,
    disableCombineTextItems: false,
  };

  const lines = (
    await Promise.all(
      Array.from({ length: doc.numPages }, (_, i) => i + 1).map((index) =>
        doc
          .getPage(index)
          .then((page) => page.getTextContent(render_options))
          .then((textContent) => {
            let lastY = 10000;
            let line = [];
            const lines = [];
            textContent.items
              .map((textItem) => {
                const tx = textItem.transform;
                const style = textContent.styles[textItem.fontName];

                // adjust for font ascent/descent
                const fontSize = Math.sqrt(tx[2] * tx[2] + tx[3] * tx[3]);

                if (style.ascent) {
                  tx[5] -= fontSize * style.ascent;
                } else if (style.descent) {
                  tx[5] -= fontSize * (1 + style.descent);
                } else {
                  tx[5] -= fontSize / 2;
                }

                const x = Math.round(tx[4] / 10);
                const y = Math.round(tx[5] / 10);
                return { ...textItem, x, y };
              })
              .sort(({ y: a }, { y: b }) => b - a)
              .forEach((textItem) => {
                const text = textItem.str;
                if (!text.trim().length) {
                  return;
                }
                const { x, y } = textItem;
                if (y < lastY) {
                  line.length && lines.push(line);
                  lastY = y;
                  line = [[x, text]];
                } else {
                  line.push([x, text]);
                }
              });
            line.length && lines.push(line);
            return lines;
          })
      )
    )
  ).reduce((p, page) => p.concat(page));

  doc.destroy();

  return { lines, pdf: { metadata, info } };
};

let dataBuffer = fs.readFileSync(
  process.argv[2] ||
    "/home/xananax/LAB REPORTS/Electrolyte Profile Wed Feb 23 2022 14_03_14 GMT+0300 (Arabian Standard Time).pdf"
);

const toNormalDate = (date) => {
  var tzoffset = new Date().getTimezoneOffset() * 60000;
  const dateStr = new Date(date - tzoffset).toISOString();
  return new Date(dateStr);
};

PDF(dataBuffer, {}).then(({ lines, pdf }) => {
  const meta = {};
  const props = {};
  let current = meta;
  const remaining = lines
    .map((line) => {
      if (line.length === 4) {
        const [[, name], [, value], [, unit], [, range]] = line;
        if (name === "Test Name" && range === "Reference Range") {
          current = props;
        } else if (current === props && /^\d+/.test(value)) {
          props[name] = {
            value: parseFloat(value),
            unit,
            range: range.split("-").map(parseFloat),
          };
        } else {
          meta[name] = value;
          meta[unit] = range;
        }
      } else if (
        line.length === 5 &&
        current === props &&
        line[1][1].length === 1
      ) {
        const [[, name], [, marker], [, value], [, unit], [, range]] = line;
        props[name] = {
          value: parseFloat(value),
          unit,
          marker,
          range: range.split("-").map(parseFloat),
        };
      } else if (
        line.length === 2 &&
        current === props &&
        line[0][0] < 10 &&
        /^\d+/.test(line[1][1])
      ) {
        const [[, name], [, value]] = line;
        props[name] = {
          value: parseFloat(value),
          unit: "rate",
        };
      } else if (line.length === 2 && current === props && line[0][0] < 10) {
        const [[, name], [, value]] = line;
        props[name] = {
          value: value,
          unit: "qualitative",
        };
      } else {
        return line;
      }
    })
    .filter(Boolean);
  console.log({ remaining });
  console.log(
    Object.fromEntries(
      Object.entries(meta).map(([k, v]) => [
        k,
        /date/i.test(k) ? toNormalDate(new Date(v)) : v,
      ])
    )
  );
  console.log(props);
});
