import App from "./App";
import React from "react";
import { StaticRouter } from "react-router-dom";
import express from "express";
import { renderToString } from "react-dom/server";
import bodyParser from "body-parser";
import cors from "cors";
import * as fs from "./fs";

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

const cssLinksFromAssets = (assets, entrypoint) => {
  return assets[entrypoint]
    ? assets[entrypoint].css
      ? assets[entrypoint].css
          .map((asset) => `<link rel="stylesheet" href="/${asset}">`)
          .join("")
      : ""
    : "";
};

const jsScriptTagsFromAssets = (assets, entrypoint, ...extra) => {
  return assets[entrypoint]
    ? assets[entrypoint].js
      ? assets[entrypoint].js
          .map((asset) => `<script src="${asset}" ${extra.join(" ")}></script>`)
          .join("")
      : ""
    : "";
};

export const renderApp = (req, res) => {
  const context = {};
  const markup = renderToString(
    <StaticRouter context={context} location={req.url}>
      <App />
    </StaticRouter>
  );
  const html = `<!doctype html>
  <html lang="">
  <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta charset="utf-8" />
      <title>PDF Annotator</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      ${cssLinksFromAssets(assets, "client")}
  </head>
  <body>
      <div id="root">${markup}</div>
      ${jsScriptTagsFromAssets(assets, "client", "defer", "crossorigin")}
  </body>
</html>`;
  return { context, html };
};

const server = express();
server
  .disable("x-powered-by")
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR));

server.use(cors());
server.get("/path/*", (req, res, next) => {
  const path = decodeURI(req.url.replace(/^\/path/, ""));
  try {
    const results = fs.readPath(path);
    res.send(results);
  } catch (e) {
    res.status(404).send({ error: e.message });
  }
});

server.get("/stream/*", (req, res, next) => {
  const path = decodeURI(req.url.replace(/^\/stream/, ""));
  try {
    fs.streamFile(path, res);
  } catch (e) {
    res.status(404).send({ error: e.message });
  }
});

server.post("/meta/*", bodyParser.json(), (req, res, next) => {
  const path = decodeURI(req.path.replace(/^\/meta/, ""));
  const meta = req.body.meta;
  console.log({ meta });
  try {
    const file = fs.writeMeta(path, meta);
    res.send({ file });
  } catch (e) {
    res.status(404).send({ error: e.message });
  }
});

server.get("/compile/*", (req, res) => {
  const path = decodeURI(req.path.replace(/^\/compile/, ""));
  const data = fs.compileDir(path);
  res.send(data);
});

server.get("/*", (req, res) => {
  const { context, html } = renderApp(req, res);
  if (context.url) {
    res.redirect(context.url);
  } else {
    res.status(200).send(html);
  }
});

export default server;
