import { saveAs } from "file-saver";

const save_csv = (rows) => {
  const csv_string = rows.map((row) => `"${row.join('", "')}"`).join("\n");
  const file = new Blob([csv_string], { type: "text/plain;charset=utf-8" });
  saveAs(file, "report.csv");
};

export const generateCSVRowsDates = ({ values, dates, tests }) => {
  const datesIndexes = {};
  const valuesIndexes = {};
  const lineSize = Object.keys(values).length + 1;
  const csv = [
    [
      "(dates)",
      ...Object.keys(values).map((column, i) => {
        valuesIndexes[column] = i + 1;
        return column.replace(/"/g, "''");
      }),
      "(files)",
    ],
    ...Object.keys(dates)
      .sort((b, a) => new Date(a).getTime() - new Date(b).getTime())
      .map((date, i) => {
        datesIndexes[date] = i + 1;
        const files = dates[date];
        const line = [...new Array(lineSize).fill(""), ...files];
        line[0] = date;
        return line;
      }),
  ];

  Object.entries(values).map(([header, values]) => {
    values
      .sort(
        ({ date: a }, { date: b }) =>
          new Date(a).getTime() - new Date(b).getTime()
      )
      .map(({ value, date }) => {
        const x = valuesIndexes[header];
        const y = datesIndexes[date];
        csv[y][x] = `${value}`;
      });
  });
  save_csv(csv);
};

export const generateCSVRowsTests = ({ values, dates, tests }) => {
  const datesIndexes = {};
  const valuesIndexes = {};
  const additionalColumns = ["values", "unit", "range", "H/L"];
  const additionalLength = additionalColumns.length;
  const lineSize = Object.keys(dates).length + additionalLength;
  const header = [
    ...additionalColumns.map((str) => `[${str}]`),
    ...Object.keys(dates)
      .sort((b, a) => new Date(a).getTime() - new Date(b).getTime())
      .map((column, i) => {
        datesIndexes[column] = i + additionalLength;
        return column.replace(/"/g, "''");
      }),
    "[files]",
  ];
  const csv = [
    header,
    ...Object.keys(values).map((rowName, i) => {
      valuesIndexes[rowName] = i + 1;
      const value = values[rowName];
      const { name, unit, range } = value[0];
      const line = [...new Array(lineSize).fill("")];
      const marker = [...new Set(value.map(({ marker }) => marker))]
        .filter((m) => m && m != "N")
        .join(",");
      line[0] = name;
      line[1] = unit;
      line[2] = range;
      line[3] = marker;
      return line;
    }),
  ];

  Object.entries(values).map(([rowName, values]) => {
    const y = valuesIndexes[rowName];
    values
      .sort(
        ({ date: a }, { date: b }) =>
          new Date(a).getTime() - new Date(b).getTime()
      )
      .map(({ value, date }) => {
        const x = datesIndexes[date];
        csv[y][x] = `${value}`;
      });
  });

  Object.entries(values).map(([rowName, values]) => {
    const files = new Set(values.map(({ file }) => file));
    const y = valuesIndexes[rowName];
    csv[y].push(...files);
  });
  save_csv(csv);
};

export const generateJSON = (props) => {
  const json_text = JSON.stringify(props, null, 2);
  const file = new Blob([json_text], { type: "text/plain;charset=utf-8" });
  saveAs(file, "report.json");
};
