import * as pdfjs from "pdfjs-dist/legacy/build/pdf.js";
import pdfjsWorker from "pdfjs-dist/legacy/build/pdf.worker.entry";
import { fileToDataBuffer } from "./fileToDataBuffer";

pdfjs.GlobalWorkerOptions.workerSrc = pdfjsWorker;

class Token {
  x = 0;
  y = 0;
  delta = 0;
  text = "";
  constructor(x, y, text) {
    this.x = x;
    this.y = y;
    this.delta = x;
    this.text = text;
  }

  get end() {
    return this.text.length + this.x;
  }

  render(withoutSpacing = false) {
    if (withoutSpacing) {
      return this.text;
    }
    return " ".repeat(this.delta) + this.text;
  }
}

class Line {
  /** @type {Token[]} */
  tokens = [];

  get x() {
    return this.tokens.length ? this.tokens[0].x : 0;
  }

  get y() {
    return this.tokens.length ? this.tokens[0].y : 0;
  }

  get length() {
    return this.tokens.length;
  }

  addToken(token) {
    this.tokens.push(token);
  }

  render(withoutSpacing = false) {
    return this.tokens.map((token) => token.render(withoutSpacing)).join(" ");
  }
}

class Page {
  /** @type {Line[]} */
  lines = [];
  currentLine = new Line();
  lastY = 0;

  storeCurrentLine() {
    if (this.currentLine.length) {
      this.lines.push(this.currentLine);
      this.currentLine = new Line();
    }
  }

  /**
   * @param {Token} token
   */
  addToken(token) {
    if (token.y > this.lastY) {
      this.storeCurrentLine();
      this.lastY = token.y;
    }
    this.currentLine.addToken(token);
  }

  /**
   * @param {Token[]} tokensList
   */
  setTokens(tokensList) {
    tokensList.forEach((token, i, arr) => {
      this.addToken(token);
      if (this.currentLine.length > 1) {
        const prev = arr[i - 1];
        token.delta = Math.max(
          token.x - (prev ? prev.x + prev.text.length : 0),
          0
        );
      }
    });
    this.storeCurrentLine();
  }

  render(withoutSpacing = false) {
    return this.lines
      .map((line, index, arr) => {
        const next = arr[index + 1];
        if (next && !withoutSpacing) {
          const distance = next.y - line.y - 1;
          const padding = "\n".replace(distance);
          return line.render(withoutSpacing) + padding;
        }
        return line.render(withoutSpacing);
      })
      .join("\n");
  }
}

class Document {
  /** @type {Page[]} */
  pages = [];

  render(withoutSpacing = false) {
    return this.pages.map((p) => p.render(withoutSpacing)).join("\n");
  }
}

const textContentToToken = (
  /** @type {import("pdfjs-dist/types/src/display/api").TextContent} */ textContent
) => {
  return textContent.items
    .map((textItem) => {
      const tx = textItem.transform;
      const style = textContent.styles[textItem.fontName];

      // adjust for font ascent/descent
      const fontSize = Math.sqrt(tx[2] * tx[2] + tx[3] * tx[3]);

      if (style.ascent) {
        tx[5] -= fontSize * style.ascent;
      } else if (style.descent) {
        tx[5] -= fontSize * (1 + style.descent);
      } else {
        tx[5] -= fontSize / 2;
      }

      const x = Math.round(tx[4] / 6);
      const y = Math.round(tx[5] / 6);
      const text = /** @type {string} */ (textItem.str);
      return { ...textItem, x, y, text };
    })
    .sort(({ y: a }, { y: b }) => b - a)
    .map((textItem, i, arr) => {
      const { x, text } = textItem;
      if (!text.trim().length) {
        return;
      }
      const y = arr[0].y - textItem.y;
      return new Token(x, y, text);
    })
    .filter(Boolean);
};

export const pdf = async (/** @type {Uint8Array} */ dataBuffer) => {
  let doc = await pdfjs.getDocument(dataBuffer).promise;

  const { metadata, info } = await doc.getMetadata();

  const render_options = {
    normalizeWhitespace: false,
    disableCombineTextItems: false,
  };

  const pages = await Promise.all(
    Array.from({ length: doc.numPages }, (_, i) => i + 1).map((index) =>
      doc
        .getPage(index)
        .then((page) => page.getTextContent(render_options))
        .then(textContentToToken)
        .then((tokens) => {
          const page = new Page();
          page.setTokens(tokens);
          return page;
        })
    )
  );

  doc.destroy();

  const document = new Document();
  document.pages = pages;

  return { document, pdf: { metadata, info } };
};

const parseFile = (/** @type {File} */ file) =>
  fileToDataBuffer(file).then(async ({ name, contents }) => ({
    name,
    ...(await pdf(contents)),
  }));

/**
 * @param {File[]} acceptedFiles
 */
export const parseFileList = async (acceptedFiles) =>
  await Promise.all(acceptedFiles.map(parseFile));
