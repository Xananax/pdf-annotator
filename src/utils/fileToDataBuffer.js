/**
 *
 * @param {File} file
 * @returns {Promise<{name:string, content: Uint8Array}>}
 */
export const fileToDataBuffer = (file) =>
  new Promise((ok, no) => {
    const reader = new FileReader();
    reader.onabort = no;
    reader.onerror = no;
    reader.onload = () =>
      ok({ name: file.name, contents: new Uint8Array(reader.result) });
    reader.readAsArrayBuffer(file);
  });

/**
 *
 * @param {File[]} files
 * @returns {Promise<Awaited<ReturnType<typeof fileToDataBuffer>>[]>}
 */
export const fileListToDataBuffer = (files) =>
  Promise.all(files.map(fileToDataBuffer));
