//@ts-check
import { useCallback, useState } from "react";
import { parseFileList } from "./pdf";
import { toast } from "react-toastify";

/**
 *
 * @param {Awaited<ReturnType<typeof parseFileList>>[0]} document
 * @returns
 */
const renderDocument = ({ document, pdf, name }) => ({
  document,
  pdf,
  name,
  text: document.render(),
});

export const usePDfParser = () => {
  const [files, setFiles] = useState(
    /** @type {ReturnType<typeof renderDocument>[]} */ ([])
  );
  const onNewFiles = useCallback(
    async (acceptedFiles) => {
      const newFiles = (await parseFileList(acceptedFiles))
        .map(renderDocument)
        .filter(({ text: a, name }) => {
          const exists = files.find(({ text: b }) => a === b);
          if (exists) {
            toast.error(`file "${name}" is already loaded, skipping`);
            return false;
          }
          return true;
        });
      setFiles([...files, ...newFiles]);
    },
    [files]
  );

  const clearFiles = () => setFiles([]);

  const removeFile = (/** @type {number} */ index) =>
    setFiles([...files.slice(0, index), ...files.slice(index + 1)]);

  return { files, onNewFiles, clearFiles, removeFile };
};
