import React from "react";
import { useDropzone } from "react-dropzone";
import { usePDfParser } from "./utils/usePDfParser";
import classNames from "classnames/bind";
import css from "./Parser.module.css";

const cx = classNames.bind(css);

export const PdfParser = () => {
  const { files, onNewFiles: onDrop } = usePDfParser();

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: "application/pdf",
  });

  return (
    <div className={cx("parser")}>
      {files.map(({ text }) => (
        <textarea key={text} defaultValue={text}></textarea>
      ))}
      <div {...getRootProps({ className: cx("dropzone", { isDragActive }) })}>
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop the files here ...</p>
        ) : (
          <p>Drag 'n' drop some files here, or click to select files</p>
        )}
      </div>
    </div>
  );
};

export default PdfParser;
