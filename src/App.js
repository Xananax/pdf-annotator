import React from "react";
import { Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Home from "./Home";
import Parser from "./Parser";
import "./App.css";

const App = () => (
  <div>
    <Switch>
      <Route path="/*" component={Parser} />
    </Switch>
    <ToastContainer />
  </div>
);

export default App;
