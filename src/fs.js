//@ts-check
import fs from "fs";
import {
  basename,
  basename as getBaseName,
  dirname,
  extname,
  join,
} from "path";
import yaml from "js-yaml";

const getStats = (fullPath) => {
  const filename = getBaseName(fullPath);
  const directory = dirname(fullPath);
  const _ext = extname(filename);
  const basename = getBaseName(filename, _ext);
  const ext = _ext.slice(1).toLowerCase();
  const title = basename.replace(/[-_+]/g, " ");
  const props = {
    basename,
    directory,
    ext,
    fullPath,
    title,
    filename,
    date: "",
    meta: {},
    isDirectory: false,
    size: 0,
    mtimeMs: 0,
    children: [],
    access: true,
  };
  try {
    const stat = fs.statSync(fullPath);
    const isDirectory = stat.isDirectory();
    const { size, mtimeMs } = stat;
    const date = new Date(mtimeMs).toISOString().split("T")[0];
    const augmentedProps = {
      ...props,
      isDirectory,
      size,
      date,
      mtimeMs,
    };
    return augmentedProps;
  } catch (e) {
    if (e.code === `ENOENT`) {
      throw e;
    }
    return { ...props, access: false };
  }
};

/**
 * @typedef {ReturnType<typeof getStats>} Stats
 */

export const readDir = (path, skipHidden = true) => {
  try {
    return fs
      .readdirSync(path)
      .map((filename) => {
        if (skipHidden && filename[0] === ".") {
          return;
        }
        const fullPath = join(path, filename);
        try {
          return getStats(fullPath);
        } catch (e) {
          console.error(e);
        }
      })
      .filter(Boolean);
  } catch (e) {
    return e;
  }
};

export const readPath = (path) => {
  const stats = getStats(path);
  if (stats.access && stats.isDirectory) {
    return { ...stats, children: readDir(path), meta: getMeta(stats) };
  } else if (stats.access) {
    return { ...stats, meta: getMeta(stats) };
  }
  return stats;
};

export const getDir = (path) => {
  const stats = getStats(path);
  if (!stats.isDirectory) {
    return readPath(dirname(path));
  }
  return stats;
};

export const streamFile = (path, res) => {
  const stats = getStats(path);
  if (stats.access && !stats.isDirectory) {
    const file = fs.createReadStream(path);
    res.setHeader("Content-Length", stats.size);
    res.setHeader("Content-Type", "application/pdf");
    //res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
    file.pipe(res);
    return;
  }
  throw new Error("not a file");
};

export const getMetaFileName = (path) => {
  const filename = basename(path);
  const directory = dirname(path);
  const metaFile = `${directory}/.__${filename}.meta.yaml`;
  return metaFile;
};

export const getMeta = (stats) => {
  if (stats) {
    const { title, date } = stats;
    const defaults = {
      title,
      date,
    };
    const metaFile = getMetaFileName(stats.fullPath);
    if (fs.existsSync(metaFile)) {
      try {
        const data = /** @type{Object} */ (
          yaml.load(fs.readFileSync(metaFile, "utf8"))
        );

        return { ...defaults, ...data };
      } catch (e) {
        console.error(e.message);
      }
    }
    return defaults;
  }
  return {};
};

export const writeMeta = (path, data) => {
  if (fs.existsSync(path)) {
    const { filename, date: file_date, title } = getStats(path);
    data = { filename, file_date, title, ...data };
    const metaFile = getMetaFileName(path);
    let str;
    try {
      str = yaml.dump(data);
    } catch (e) {
      console.error(e.message);
      return false;
    }
    try {
      fs.writeFileSync(metaFile, str, "utf-8");
      return metaFile;
    } catch (e) {
      console.error(e.message);
      return false;
    }
  }
  return false;
};

export const compileDir = (path) => {
  const dir = getStats(path);
  if (!dir.access || !dir.isDirectory) {
    throw new Error(`not a directory`);
  }
  const values = {};
  const dates = {};
  const tests = readDir(path).reduce((arr, stats) => {
    try {
      getStats(getMetaFileName(stats.fullPath));
      const meta = getMeta(stats);
      const date = meta.date.split("T")[0];
      const file = stats.filename;
      dates[date] = dates[date] || [];
      dates[date].push(file);
      if ("quality" in meta && Object.entries(meta.quality).length > 0) {
        Object.entries(meta.quality).map(
          ([name, { value, reference, marker }]) => {
            const key = `${name}_q_[${reference}]`;
            values[key] = values[key] || [];
            values[key].push({
              value,
              name,
              date,
              file,
              range: reference,
              marker,
              unit: "qualitative",
            });
          }
        );
        arr.push(meta.quality);
      }
      if ("tests" in meta && Object.entries(meta.tests).length > 0) {
        Object.entries(meta.tests).map(
          ([testName, { value, unit, range, marker }]) => {
            const _range = range
              ? range
                  .split("-")
                  .map((s) => s.trim())
                  .join("-")
              : "";
            const key = `${testName}[${unit}][${_range}]`;
            values[key] = values[key] || [];
            values[key].push({
              value,
              name: testName,
              date,
              file,
              range,
              marker,
              unit,
            });
          }
        );
        arr.push(meta.tests);
      }
      //return { ...meta, fullPath: stats.fullPath };
    } catch (e) {}
    return arr;
  }, []);

  return { values, dates, tests };
};
