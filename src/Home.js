import React, { useEffect, useState, useCallback } from "react";
import { Link, withRouter, Prompt } from "react-router-dom";
import DatePicker from "react-date-picker";
import fetch from "cross-fetch";
import { useHotkeys } from "react-hotkeys-hook";
import { ToastContainer, toast } from "react-toastify";
import * as chrono from "chrono-node";
import {
  generateCSVRowsDates,
  generateCSVRowsTests,
  generateJSON,
} from "./generateCSV";
import "./Home.css";
import "react-toastify/dist/ReactToastify.css";

const getUrl = (path) =>
  `http://${process.env.HOST}:${process.env.PORT}/${path}`;

export const useDebouncedEffect = (effect, deps, delay) => {
  useEffect(() => {
    const handler = setTimeout(() => effect(), delay);

    return () => clearTimeout(handler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [...(deps || []), delay]);
};

const toNormalDate = (date) => {
  var tzoffset = new Date().getTimezoneOffset() * 60000;
  const dateStr = new Date(date - tzoffset).toISOString();
  return new Date(dateStr);
};

const useFileSystem = () => {
  const [files, setFiles] = useState([]);
  const [dir, setDir] = useState(null);
  const [file, setFile] = useState(null);
  const [meta, setMeta] = useState({});
  const [tests, setTests] = useState({});
  const [qTests, setQTests] = useState({});
  const [isDirty, setIsDirty] = useState(false);
  const [isSavingMeta, setIsSavingMeta] = useState(false);
  const startDirty = () => {
    setIsDirty(true);
  };
  const stopDirty = () => {
    setIsDirty(false);
  };
  const setMetaField = (name, value) => {
    if (meta && name in meta && meta[name] === value) {
      return;
    }
    const newMeta = { ...meta, [name]: value };
    setMeta(newMeta);
    startDirty();
  };
  const clearAllTests = () => {
    setTests({});
    setIsDirty(true);
  };
  const clearAllQTests = () => {
    setQTests({});
    setIsDirty(true);
  };
  const setTestField = (name, value, unit, range, marker) => {
    if (
      tests &&
      name in tests &&
      tests[name].value === value &&
      tests[name].range === range &&
      tests[name].marker === marker
    ) {
      return;
    }
    setTests((prev) => ({ ...prev, [name]: { value, unit, range, marker } }));
    startDirty();
  };
  const setQTestField = (name, value, reference, marker) => {
    if (
      qTests &&
      name in qTests &&
      qTests[name].value === value &&
      qTests[name].reference === reference &&
      qTests[name].marker === marker
    ) {
      return;
    }
    setQTests((prev) => ({ ...prev, [name]: { value, marker, reference } }));
    startDirty();
  };
  const removeMetaField = (name) => {
    if (name in meta) {
      const { [name]: _remove, ...rest } = meta;
      setMeta(rest);
      startDirty();
    }
  };
  const removeTestField = (name) => {
    if (name in tests) {
      const { [name]: _remove, ...rest } = tests;
      setTests(rest);
      startDirty();
    }
  };
  const removeQTestField = (name) => {
    if (name in qTests) {
      const { [name]: _remove, ...rest } = tests;
      setQTests(rest);
      startDirty();
    }
  };
  const saveMeta = () => {
    if (!file) {
      toast("No file selected!");
      return;
    }
    if (!isDirty) {
      //toast("No changes to save");
      return;
    }
    if (isSavingMeta) {
      toast("already saving");
      return;
    }
    setIsSavingMeta(true);
    const processedMeta = Object.fromEntries(
      Object.entries(meta).map(([key, value]) => {
        //if (/date/i.test(key)) {
        //  return [key, new Date(value).getTime()];
        //}
        return [key, value];
      })
    );
    processedMeta.tests = tests;
    processedMeta.quality = qTests;
    const body = JSON.stringify({ meta: processedMeta });
    const url = getUrl(`meta${file.fullPath}`);
    fetch(url, {
      method: "POST",
      body,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((results) => results.json())
      .then((stat) => {
        toast("Metadata saved");
      })
      .catch((e) => console.error(e))
      .finally(() => {
        setIsSavingMeta(false);
        stopDirty();
      });
  };

  useDebouncedEffect(saveMeta, [isDirty, meta, tests, qTests], 500);
  const closeFile = () => {
    setMeta({});
    setFile(null);
  };
  const request = (pathname, replace = true) => {
    const url = getUrl(`path${pathname}`);
    fetch(url)
      .then((results) => results.json())
      .then((stat) => {
        if (stat.isDirectory) {
          setFiles(stat.children ?? []);
          setDir(stat);
          if (replace) {
            closeFile();
          }
        } else {
          setFile(stat);
          const {
            tests: newTests = {},
            quality: newQTests = {},
            filename: _,
            file_date: __,
            ..._meta
          } = stat.meta;
          const newMeta = Object.fromEntries(
            Object.entries(_meta).map(([key, value]) => {
              if (/date/i.test(key)) {
                return [key, new Date(value)];
              }
              if (/\(.*?\)/i.test(key)) {
                return [key, parseFloat(value)];
              }
              return [key, value];
            })
          );
          setMeta(newMeta);
          setTests(newTests);
          setQTests(newQTests);
          request(stat.directory, false);
        }
      })
      .catch((e) => console.error(e));
  };
  return {
    dir,
    file,
    files,
    request,
    setMetaField,
    meta,
    isDirty,
    saveMeta,
    isSavingMeta,
    removeMetaField,
    closeFile,
    setTestField,
    removeTestField,
    clearAllTests,
    tests,
    setQTestField,
    removeQTestField,
    clearAllQTests,
    qTests,
  };
};

const Field = ({ name, value, onChange, onRemove }) => {
  const [timer, setTimer] = useState(0);
  const type = /date/i.test(name)
    ? "date"
    : /\(.*?\)/i.test(name)
    ? "number"
    : "text";
  if (type === "date") {
    return (
      <label className="field">
        <span className="field-name">{name}</span>
        <DatePicker
          locale=""
          onChange={(date) => onChange(name, toNormalDate(date))}
          value={value}
          format="dd-MMM-y"
          clearIcon={null}
        />
        <input
          type="text"
          placeholder="(paste a date here)"
          onChange={({ target }) => {
            clearTimeout(timer);
            setTimer(
              setTimeout(() => {
                const date = chrono.parseDate(target.value);
                if (date) {
                  target.value = "";
                  onChange(name, toNormalDate(date));
                }
              }, 300)
            );
          }}
        />
        <button className="deleteField" onClick={onRemove}>
          x
        </button>
      </label>
    );
  }
  return (
    <label className="field">
      <span className="field-name">{name}</span>
      <input
        name={name}
        value={value}
        type={type}
        onChange={({ target: { value } }) => onChange(name, value)}
      />
      <button className="deleteField" onClick={onRemove}>
        x
      </button>
    </label>
  );
};

const QField = ({ name, value, reference, marker, onChange, onRemove }) => {
  return (
    <label className="field">
      <span className="field-name">{name}</span>
      <input
        name={name}
        value={value}
        onChange={({ target: { value } }) => onChange(name, value)}
      />
      <span>{marker}</span>
      <span>{reference}</span>
      <button className="deleteField" onClick={onRemove}>
        x
      </button>
    </label>
  );
};

const TestField = ({
  name,
  value,
  unit,
  range,
  marker,
  onRemove,
  onChange,
}) => {
  return (
    <label className="field">
      <span className="field-name">{name}</span>
      <input
        className="test-input"
        name={name}
        value={value}
        type={"number"}
        onChange={({ target: { value } }) =>
          onChange(name, parseFloat(value), unit)
        }
      />
      {marker && marker !== "N" && (
        <span className="field-marker">{marker}</span>
      )}
      <span className="field-measure">
        <span className="field-unit">{unit}</span>
        <span className="field-range">{range}</span>
      </span>
      <button className="deleteField" onClick={onRemove}>
        x
      </button>
    </label>
  );
};

const parseQLine = (/** @type {string} */ line) => {
  const [name, value, refOrMarker, marker] = line
    .split("\\")
    .map((str) => str.trim());
  if (!name) {
    return null;
  }
  if (refOrMarker && refOrMarker.length === 1) {
    return { name, value, reference: null, marker: refOrMarker };
  }
  return { name, value, reference: refOrMarker, marker };
};

const parseLine = (/** @type{ string} */ line) => {
  if (line.indexOf("|") > -1) {
    const [name, amount, unit, range, marker = "N"] = line
      .split("|")
      .map((chunk) => chunk.trim());
    if (!title) {
      return null;
    }
    const value = parseFloat(amount);
    return { name, value, unit, range };
  } else {
    const {
      groups: { name, marker = "N", amount, unit, range },
    } = line.match(
      /(?<name>(?:[^\s]+ )+?)(?:(?<marker>H|L)\s)?(?<amount>\d+(?:\.\d+)?)\s+(?<unit>(?:\d+\s*\^)|(?:\w.+?)|%)(?:\s+(?<range>\d+(?:\.\d+)?\s?-\s?\d+(?:\.\d+)?))?(?:\n|$)/
    ) || { groups: {} };
    if (!name) {
      return null;
    }
    const value = parseFloat(amount);
    return {
      name: name.trim(),
      marker: marker.trim(),
      value,
      unit: unit.trim(),
      range: range.trim(),
    };
  }
};

const Home = ({ location: { pathname } }) => {
  const {
    dir,
    file,
    files,
    request,
    setMetaField,
    meta,
    isDirty,
    saveMeta,
    isSavingMeta,
    removeMetaField,
    closeFile,
    setTestField,
    removeTestField,
    clearAllTests,
    tests,
    setQTestField,
    removeQTestField,
    clearAllQTests,
    qTests,
  } = useFileSystem();
  useHotkeys("ctrl+s", (evt) => {
    evt.preventDefault();
    saveMeta();
  });
  const src = file && getUrl(`stream/${file.fullPath}`);

  let first = true;
  const selectedRef = useCallback(
    (node) => {
      if (node !== null && first) {
        first = false;
        node.scrollIntoView();
      }
    },
    [first]
  );
  const [currentMetaName, setCurrentMetaName] = useState("");
  const [currentMetaValue, setCurrentMetaValue] = useState("");
  const [currentLineValue, setCurrentLineValue] = useState("");
  const [currentQLineValue, setCurrentQLineValue] = useState("");
  const [columns, setColumns] = useState(true);

  const addNewField = (evt) => {
    evt.preventDefault();
    setMetaField(currentMetaName, currentMetaValue);
    setCurrentMetaName("");
    setCurrentMetaValue("");
    evt.target.reset();
  };

  const addNewFieldFromLine = (evt) => {
    evt.preventDefault();
    const remaining = currentLineValue
      .split("\n")
      .map((line) => {
        if (!line) {
          return;
        }
        const parsed = parseLine(line);
        if (!parsed) {
          return line;
        }
        const { name, value, unit, range, marker } = parsed;
        setTestField(name, value, unit, range, marker);
      })
      .filter(Boolean);
    setCurrentLineValue(remaining.join("\n"));
  };

  const addNewQFieldFromLine = (evt) => {
    evt.preventDefault();
    const remaining = currentQLineValue
      .split("\n")
      .map((line) => {
        if (!line) {
          return;
        }
        const parsed = parseQLine(line);
        if (!parsed) {
          return line;
        }
        const { name, value, reference, marker } = parsed;
        setQTestField(name, value, reference, marker);
      })
      .filter(Boolean);
    setCurrentQLineValue(remaining.join("\n"));
  };

  useEffect(() => request(pathname), [pathname]);

  return (
    <div className={`main columns-${file ? "3" : "1"}`}>
      <Prompt
        when={isDirty}
        message="You have unsaved changes, are you sure you want to leave?"
      />
      <div className={`column${columns ? "" : " closed"}`}>
        <div>
          <h1>{dir && dir.basename}</h1>
          {files
            .filter((stat) => stat.isDirectory)
            .map((stat, i) => (
              <div key={stat.fullPath}>
                <Link to={stat.fullPath}>{stat.basename}/</Link>
              </div>
            ))}
        </div>
        <hr />
        <div>
          {files
            .filter((stat) => !stat.isDirectory && stat.ext === "pdf")
            .map((stat, i) => (
              <div
                className={`file${
                  file && file.fullPath === stat.fullPath ? " current" : ""
                }`}
                ref={
                  file && file.fullPath === stat.fullPath ? selectedRef : null
                }
                key={stat.fullPath}
              >
                <Link to={stat.fullPath}>
                  {stat.basename.replace(/\s*[\+_-]\s*/g, " ")}
                </Link>
              </div>
            ))}
        </div>
      </div>
      {file && (
        <div className="column">
          <h3>{file && file.basename}</h3>
          <h2>{meta && meta.title}</h2>
          {meta &&
            Object.entries(meta).map(([key, val]) => (
              <Field
                key={key}
                name={key}
                value={val}
                onChange={setMetaField}
                onRemove={() => removeMetaField(key)}
              />
            ))}
          <form className="field" onSubmit={addNewField}>
            <input
              placeholder="new field"
              value={currentMetaName}
              onChange={({ target: { value } }) =>
                setCurrentMetaName(value.trim())
              }
            />
            <input
              placeholder="new value"
              value={currentMetaValue}
              onChange={({ target: { value } }) =>
                setCurrentMetaValue(value.trim())
              }
            />
            <button>ok</button>
          </form>
          <div>
            Qualitative -
            <button onClick={clearAllQTests}>
              clear all qualitative tests
            </button>
          </div>
          {qTests &&
            Object.entries(qTests).map(
              ([key, { value, marker, reference }]) => (
                <QField
                  key={key}
                  name={key}
                  value={value}
                  marker={marker}
                  reference={reference}
                  onChange={setQTestField}
                  onRemove={() => removeQTestField(key)}
                />
              )
            )}
          <form className="field" onSubmit={addNewQFieldFromLine}>
            <textarea
              placeholder="add a full line"
              value={currentQLineValue}
              onChange={({ target: { value } }) => setCurrentQLineValue(value)}
            />
            <input type="submit" value="ok" />
            <input
              type="button"
              onClick={(evt) => setCurrentQLineValue("")}
              value="clear"
            />
          </form>

          <div>
            Tests -<button onClick={clearAllTests}>clear all tests</button>
          </div>
          {tests &&
            Object.entries(tests).map(
              ([key, { value, unit, range, marker }]) => (
                <TestField
                  key={key}
                  name={key}
                  value={value}
                  unit={unit}
                  range={range}
                  marker={marker}
                  onChange={setTestField}
                  onRemove={() => removeTestField(key)}
                />
              )
            )}
          <form className="field" onSubmit={addNewFieldFromLine}>
            <textarea
              placeholder="add a full line"
              value={currentLineValue}
              onChange={({ target: { value } }) => setCurrentLineValue(value)}
            />
            <input type="submit" value="ok" />
            <input
              type="button"
              onClick={(evt) => setCurrentLineValue("")}
              value="clear"
            />
          </form>
          <button disabled={isSavingMeta} onClick={saveMeta}>
            Save
          </button>
        </div>
      )}
      {file && <iframe className="column" src={src}></iframe>}
      {file && (
        <button
          style={{ position: "absolute", top: 0, right: 0 }}
          onClick={closeFile}
        >
          x
        </button>
      )}
      <div className="top-nav">
        <button onClick={() => setColumns(!columns)}>|||</button>
        <button
          onClick={() =>
            fetch(getUrl(`compile${dir.fullPath}`))
              .then((res) => res.json())
              .then(generateCSVRowsTests)
          }
        >
          CSV cols dates
        </button>
        <button
          onClick={() =>
            fetch(getUrl(`compile${dir.fullPath}`))
              .then((res) => res.json())
              .then(generateCSVRowsDates)
          }
        >
          CSV rows dates
        </button>
        <button
          onClick={() =>
            fetch(getUrl(`compile${dir.fullPath}`))
              .then((res) => res.json())
              .then(generateJSON)
          }
        >
          json
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default withRouter(Home);
